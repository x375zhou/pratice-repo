using Godot;
using System;

public class RootNode : Node2D
{
    // Declare member variables here. Examples:
    // private int a = 2;
    // private string b = "text";

    // Called when the node enters the scene tree for the first time.
    public override void _Ready()
    {
        GD.Print("Hello, Godot!");

        int secretOfLife = 0;
        GD.Print("This is a practice repo!");

        secretOfLife = 42;

        GD.Print("I wonder what the secret of life is?");

        GD.Print("Jun 29 2020");
    }

//  // Called every frame. 'delta' is the elapsed time since the previous frame.
//  public override void _Process(float delta)
//  {
//      
//  }
}